package br.ucsal.bes20191.bd2.varejista.converter;

import javax.persistence.AttributeConverter;

import br.ucsal.bes20191.bd2.varejista.enums.SituacaoVendedorEnum;

public class SituacaoVendedorConverter implements AttributeConverter<SituacaoVendedorEnum, String> {

	@Override
	public String convertToDatabaseColumn(SituacaoVendedorEnum situacaoVendedorEnum) {
		return situacaoVendedorEnum != null ? situacaoVendedorEnum.getCodigo() : null;
	}

	@Override
	public SituacaoVendedorEnum convertToEntityAttribute(String dbData) {
		return dbData != null ? SituacaoVendedorEnum.valueOfCodigo(dbData) : null;
	}

}
