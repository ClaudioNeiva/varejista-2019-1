package br.ucsal.bes20191.bd2.varejista.domain;

import java.sql.Date;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name="tab_administrativo")

//quando strategy=InheritanceType.SINGLE_TABLE)
//@DiscriminatorValue("ADM")
public class Administrativo extends Funcionario {

	private Integer turno;// - integer - not null

	public Administrativo() {
	}
	
	public Administrativo(String cpf, String nome, String rg, String rgOrgaoExpedidor, String rgUf,
			List<String> telefones, Date dataNascimento, Endereco endere�o, Integer turno) {
		super(cpf, nome, rg, rgOrgaoExpedidor, rgUf, telefones, dataNascimento, endere�o);
		this.turno = turno;
	}

	public Integer getTurno() {
		return turno;
	}

	public void setTurno(Integer turno) {
		this.turno = turno;
	}

	@Override
	public String toString() {
		return "Administrativo [turno=" + turno + ", toString()=" + super.toString() + "]";
	}

}
