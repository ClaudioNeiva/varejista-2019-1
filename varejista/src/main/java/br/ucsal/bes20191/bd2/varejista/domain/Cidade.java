package br.ucsal.bes20191.bd2.varejista.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity(name = "tab_cidade")
public class Cidade {

	@Id
	private String sigla;// - char(3) - not null

	private String nome;// - varchar(40) - not null

	@ManyToOne(cascade = CascadeType.ALL)
	private Estado estado;// - char(2) - not null

	public Cidade() {
	}

	public Cidade(String sigla, String nome, Estado estado) {
		super();
		this.sigla = sigla;
		this.nome = nome;
		this.estado = estado;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Cidade [sigla=" + sigla + ", nome=" + nome + ", estado=" + estado + "]";
	}

}
