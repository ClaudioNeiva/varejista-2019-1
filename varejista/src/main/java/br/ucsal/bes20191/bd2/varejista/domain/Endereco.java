package br.ucsal.bes20191.bd2.varejista.domain;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class Endereco {

	private String logradouro;// - varchar(40) - not null

	private String bairro;// - varchar(40) - not null

	@ManyToOne
	private Cidade cidade;// - char(3) - not null

	public Endereco() {
	}
	
	public Endereco(String logradouro, String bairro, Cidade cidade) {
		super();
		this.logradouro = logradouro;
		this.bairro = bairro;
		this.cidade = cidade;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	@Override
	public String toString() {
		return "Endereco [logradouro=" + logradouro + ", bairro=" + bairro + ", cidade=" + cidade + "]";
	}

}
