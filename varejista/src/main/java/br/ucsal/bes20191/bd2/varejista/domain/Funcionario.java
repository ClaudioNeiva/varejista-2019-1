package br.ucsal.bes20191.bd2.varejista.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity(name = "tab_funcionario")
@Table(uniqueConstraints = {
		@UniqueConstraint(name = "un_funcionario_rg", columnNames = { "rg", "rg_orgao_expedidor", "rg_uf" }) })
@Inheritance(strategy = InheritanceType.JOINED)

//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

// @Inheritance(strategy=InheritanceType.SINGLE_TABLE)
// @DiscriminatorColumn(name="tipo", columnDefinition="char(3)")
// @DiscriminatorValue("FNC")
public class Funcionario {

	@Id
	@Column(columnDefinition = "char(11)")
	private String cpf;

	@Column(length = 40, nullable = false)
	private String nome;

	@Column(columnDefinition = "char(12)", nullable = true)
	private String rg;

	@Column(name = "rg_orgao_expedidor", length = 20, nullable = true)
	private String rgOrgaoExpedidor;

	@Column(name = "rg_uf", columnDefinition = "char(2)", nullable = true)
	private String rgUf;

	@ElementCollection
	@CollectionTable(name = "tab_funcionario_telefone", joinColumns = @JoinColumn(name = "cpf"))
	@Column(name = "telefone", length = 12, nullable = false)
	private List<String> telefones;// - cada item varchar(12)

	@Temporal(TemporalType.DATE)
	private Date dataNascimento;// - date - not null

	@Embedded
	private Endereco enderešo;// - embedded - not null

	public Funcionario() {
	}

	public Funcionario(String cpf, String nome, String rg, String rgOrgaoExpedidor, String rgUf, List<String> telefones,
			Date dataNascimento, Endereco enderešo) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.rg = rg;
		this.rgOrgaoExpedidor = rgOrgaoExpedidor;
		this.rgUf = rgUf;
		this.telefones = telefones;
		this.dataNascimento = dataNascimento;
		this.enderešo = enderešo;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getRgOrgaoExpedidor() {
		return rgOrgaoExpedidor;
	}

	public void setRgOrgaoExpedidor(String rgOrgaoExpedidor) {
		this.rgOrgaoExpedidor = rgOrgaoExpedidor;
	}

	public String getRgUf() {
		return rgUf;
	}

	public void setRgUf(String rgUf) {
		this.rgUf = rgUf;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Endereco getEnderešo() {
		return enderešo;
	}

	public void setEnderešo(Endereco enderešo) {
		this.enderešo = enderešo;
	}

	@Override
	public String toString() {
		return "Funcionario [cpf=" + cpf + ", nome=" + nome + ", rg=" + rg + ", rgOrgaoExpedidor=" + rgOrgaoExpedidor
				+ ", rgUf=" + rgUf + ", telefones=" + telefones + ", dataNascimento=" + dataNascimento + ", enderešo="
				+ enderešo + "]";
	}

}
