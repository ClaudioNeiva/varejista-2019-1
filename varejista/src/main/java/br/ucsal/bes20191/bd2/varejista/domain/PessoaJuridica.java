package br.ucsal.bes20191.bd2.varejista.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity(name = "tab_pessoa_juridica")
public class PessoaJuridica {

	@Id
	private String cnpj;// - char(11) - not null

	private String nome;// - varchar(40) - not null

	@ManyToMany
	private List<RamoAtividade> ramosAtividade;//

	private Double faturamento;// - num�rica(10,2) - not null

	@ManyToMany
	private List<Vendedor> vendedores;//

	public PessoaJuridica() {
	}
	
	public PessoaJuridica(String cnpj, String nome, List<RamoAtividade> ramosAtividade, Double faturamento,
			List<Vendedor> vendedores) {
		super();
		this.cnpj = cnpj;
		this.nome = nome;
		this.ramosAtividade = ramosAtividade;
		this.faturamento = faturamento;
		this.vendedores = vendedores;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<RamoAtividade> getRamosAtividade() {
		return ramosAtividade;
	}

	public void setRamosAtividade(List<RamoAtividade> ramosAtividade) {
		this.ramosAtividade = ramosAtividade;
	}

	public Double getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(Double faturamento) {
		this.faturamento = faturamento;
	}

	public List<Vendedor> getVendedores() {
		return vendedores;
	}

	public void setVendedores(List<Vendedor> vendedores) {
		this.vendedores = vendedores;
	}

	@Override
	public String toString() {
		return "PessoaJuridica [cnpj=" + cnpj + ", nome=" + nome + ", ramosAtividade=" + ramosAtividade
				+ ", faturamento=" + faturamento + ", vendedores=" + vendedores + "]";
	}

}
