package br.ucsal.bes20191.bd2.varejista.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "tab_ramo_atividade")
public class RamoAtividade {

	@Id
	private Integer id;// - integer (sequence) - not null

	private String nome;// - varchar(40) - not null

	public RamoAtividade() {
	}

	public RamoAtividade(Integer id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "RamoAtividade [id=" + id + ", nome=" + nome + "]";
	}

}
