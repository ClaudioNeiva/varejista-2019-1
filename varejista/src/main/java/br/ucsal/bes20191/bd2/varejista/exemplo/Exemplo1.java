package br.ucsal.bes20191.bd2.varejista.exemplo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;

import br.ucsal.bes20191.bd2.varejista.domain.Cidade;
import br.ucsal.bes20191.bd2.varejista.domain.Estado;

public class Exemplo1 {

	public static void main(String[] args) {

		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory("varejista");
			EntityManager em = emf.createEntityManager();

			em.getTransaction().begin();

			// Estado estadoBA = new Estado("BA", "Bahia");
			// Cidade cidadeSSA = new Cidade("SSA", "Salvador", estadoBA);
			// em.persist(estadoBA);
			// em.persist(cidadeSSA);

			Cidade cidadeSSA = new Cidade("SSA", "Salvador", new Estado("BA", "Bahia"));
			em.persist(cidadeSSA);

			em.getTransaction().commit();

			em.close();
		} catch (Exception e) {
			logger.error("Erro: ", e);
		} finally {
			if (emf != null) {
				emf.close();
			}
		}
	}

	private static final Logger logger = Logger.getLogger(Exemplo1.class);

}
